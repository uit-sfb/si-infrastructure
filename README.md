## si-infrastructure

Restructure, add or delete as make sense

### Ansible playbooks

**install_TIG_stack.yml**

- Add relevant IPs to hosts file
- Add hosts file to .gitignore
- If your associated ssh-key has a password, add to SSH-agent with `ssh-add <path_to_key>` (for convenience)
- Run with: `ansible-playbook --private-key <PATH> -i hosts -l <TARGET> install_TIGststack.yml`  
  -- available targets are currently **metrics**, **frontend** and **monitor**. **production** targets all machines at once

based on https://portal.influxdata.com/downloads/
